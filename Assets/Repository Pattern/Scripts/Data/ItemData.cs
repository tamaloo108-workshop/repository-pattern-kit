﻿[System.Serializable]
public class ItemData : BaseData
{
    public string ItemName;
    public float Values;

    public ItemData(string id, float values, string itemName)
    {
        Id = id;
        Values = values;
        ItemName = itemName;
    }

}

public class BaseData
{
    public string Id;
}