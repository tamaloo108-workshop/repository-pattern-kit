﻿using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class GameData
{
    public List<InventoryData> Inventories;
    public List<ShopData> Shops;

    public GameData()
    {
        Inventories = new List<InventoryData>();
        Shops = new List<ShopData>();
    }
}
