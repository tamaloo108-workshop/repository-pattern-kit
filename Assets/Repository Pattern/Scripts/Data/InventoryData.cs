﻿using System.Collections.Generic;
using System.Linq;

[System.Serializable]
public class InventoryData : BaseData
{
    public List<ItemData> Items = new List<ItemData>();

    ItemData _tempItem;

    public InventoryData(string id, List<ItemData> items)
    {
        Id = id;
        Items = items;
    }
    public ItemData GetItem(string id)
    {
        _tempItem = Items.FirstOrDefault(x => x.Id == id);
        return _tempItem;
    }
}
