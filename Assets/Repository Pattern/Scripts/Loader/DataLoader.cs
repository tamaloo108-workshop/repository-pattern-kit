﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public abstract class DataLoader : MonoBehaviour
{
    protected GameData data = new GameData();

    public abstract Task Load();
    public abstract Task Save();

    public List<T> Set<T>()
    {
        if (typeof(T) == typeof(InventoryData))
        {
            return data.Inventories as List<T>;
        }

        if (typeof(T) == typeof(ShopData))
        {
            return data.Shops as List<T>;
        }

        return null;

    }
}
