﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;

public class JSONDataLoader : DataLoader
{
    [SerializeField] private string folderName;
    [SerializeField] private string fileName;

    string fullPath => Path.Combine(Application.streamingAssetsPath, folderName, fileName);
    string folderPath => Path.Combine(Application.streamingAssetsPath, folderName);

    string currentDir;
    string pathCurDir;
    string json;
    DirectoryInfo _info;
    StreamWriter writer;
    StreamReader reader;

    public override async Task Load()
    {
        await ValidateFolder();

        if (!File.Exists(fullPath)) return;
        reader = new StreamReader(fullPath);
        json = await reader.ReadToEndAsync();
        JsonUtility.FromJsonOverwrite(json, data);
        reader.Close();
    }

    public override async Task Save()
    {
        await ValidateFolder();
        json = JsonUtility.ToJson(data, true);
        using (writer = new StreamWriter(fullPath))
        {
            await writer.WriteAsync(json);
            await writer.FlushAsync();

            writer.Close();
        }

        await Task.Delay(10);
    }


    async Task ValidateFolder()
    {
        if (File.Exists(fullPath)) return;

        currentDir = Directory.GetCurrentDirectory();
        pathCurDir = Path.Combine(currentDir, Application.streamingAssetsPath);

        if (!Directory.Exists(pathCurDir))
        {
            _info = Directory.CreateDirectory(pathCurDir);
        }

        if (!Directory.Exists(folderPath))
        {
            _info = Directory.CreateDirectory(folderPath);
        }

        await Task.Delay(10);
    }

}
