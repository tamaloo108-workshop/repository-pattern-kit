﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitOfWork : MonoBehaviour
{
    [SerializeField] DataLoader loader;
    [SerializeField] ShopRepository shopRepository;
    [SerializeField] InventoryRepository inventoryRepository;

    public ShopRepository ShopRepository => shopRepository;
    public InventoryRepository InventoryRepository => inventoryRepository;

    public void Save()
    {
        loader.Save();
    }

    public void Load()
    {
        loader.Load();
    }

}
