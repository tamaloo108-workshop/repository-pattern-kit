﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class Repository<T> : MonoBehaviour, IRepository<T> where T : class
{

    [SerializeField] DataLoader loader;

    List<T> Entities => loader.Set<T>();

    public void Add(T entity)
    {
        Entities.Add(entity);
    }

    public void AddRange(IEnumerable<T> entities)
    {
        Entities.AddRange(entities);
    }

    public IEnumerable<T> Find(Func<T, bool> predicate)
    {
        return Entities.Where(predicate);
    }

    public T Get(Func<T, bool> predicate)
    {
        return Entities.FirstOrDefault(predicate);
    }

    public IEnumerable<T> GetAll()
    {
        return Entities;
    }

    public void Remove(T entity)
    {
        Entities.Remove(entity);
    }

    public void RemoveRange(int index, int count)
    {
        Entities.RemoveRange(index, count);
    }
}
